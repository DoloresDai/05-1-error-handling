package com.twuc.webApp;

import com.twuc.webApp.model.Message;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class ErrorHandingControllerTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_return_status_500_when_method_throw_Exception() throws Exception {
        ResponseEntity<String> forEntity = testRestTemplate
                .getForEntity("/api/errors/default", String.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, forEntity.getStatusCode());
    }

    @Test
    void should_handle_IllegalArgumentException_and_status_is_500() {
        ResponseEntity<String> forEntity = testRestTemplate
                .getForEntity("/api/errors/illegal-argument", String.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, forEntity.getStatusCode());
        String body = forEntity.getBody();
        assertEquals("Something wrong with the argument", body);
    }

    @Test
    void should_return_the_same_string_when_handle_NullPointerException_and_ArithmeticException() {
        ResponseEntity<String> forEntityNullPointer = testRestTemplate.getForEntity("/api/errors/null-pointer", String.class);
        ResponseEntity<String> forEntityArithmetic = testRestTemplate.getForEntity("/api/errors/arithmetic", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, forEntityNullPointer.getStatusCode());
        assertEquals(HttpStatus.I_AM_A_TEAPOT, forEntityArithmetic.getStatusCode());
        assertEquals("Something wrong with the argument", forEntityNullPointer.getBody());
        assertEquals("Something wrong with the argument", forEntityArithmetic.getBody());
    }

    @Test
    void should_handle_Two_IllegalArgumentException() {
        ResponseEntity<String> forEntitySister = testRestTemplate.getForEntity("/api/sister-errors/illegal-argument", String.class);
        ResponseEntity<String> forEntityBrother = testRestTemplate.getForEntity("/api/brother-errors/illegal-argument", String.class);

        assertEquals(HttpStatus.I_AM_A_TEAPOT, forEntitySister.getStatusCode());
        assertEquals(HttpStatus.I_AM_A_TEAPOT, forEntityBrother.getStatusCode());

        assertEquals(MediaType.APPLICATION_JSON_UTF8, forEntitySister.getHeaders().getContentType());
        assertEquals(MediaType.APPLICATION_JSON_UTF8, forEntityBrother.getHeaders().getContentType());

        assertEquals("{\"message\":\"Something wrong with brother or sister.\"}", forEntitySister.getBody());
        assertEquals("{\"message\":\"Something wrong with brother or sister.\"}", forEntityBrother.getBody());
    }
}
