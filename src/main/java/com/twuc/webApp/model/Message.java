package com.twuc.webApp.model;

public class Message {
    public Message(String message) {
        this.message = message;
    }

    private String message;

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Something wrong with brother or sister.";
    }
}
