package com.twuc.webApp.controller;

import com.twuc.webApp.model.Message;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
@RestController
@RequestMapping("/api")
public class ErrorHandingController {
    @GetMapping("/default")
    public ResponseEntity<String> throwException() {
        throw new RuntimeException();
    }

    @GetMapping("/errors/illegal-argument")
    public void throwIllegalArgumentException() {
        throw new IllegalArgumentException();
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handelIllegalArgumentException() {
        return ResponseEntity
                .status(500)
                .body("Something wrong with the argument");
    }

    @GetMapping("/errors/null-pointer")
    public void throwNullPointerException() {
        throw new NullPointerException();
    }

    @GetMapping("/errors/arithmetic")
    public void throwArithmeticException() {
        throw new ArithmeticException();
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleRuntimeException() {
        return ResponseEntity.status(418)
                .body("Something wrong with the argument");
    }

    @GetMapping("*-errors/illegal-argument")
    public void throwIllegalArgumentExceptionSister() {
        throw new IllegalArgumentException();
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Message> handleTwoIllegalArgumentException() {
        return ResponseEntity
                .status(418)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(new Message("Something wrong with brother or sister."));
    }
}

